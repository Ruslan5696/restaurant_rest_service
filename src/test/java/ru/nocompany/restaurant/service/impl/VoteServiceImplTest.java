package ru.nocompany.restaurant.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.nocompany.restaurant.AbstractTest;
import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.model.entities.Role;
import ru.nocompany.restaurant.model.entities.User;
import ru.nocompany.restaurant.repository.RestaurantRep;
import ru.nocompany.restaurant.repository.UserRep;
import ru.nocompany.restaurant.service.VoteService;



@WebAppConfiguration
//@Transactional
public class VoteServiceImplTest extends AbstractTest {
  private static final Logger LOG = LoggerFactory.getLogger(VoteServiceImplTest.class);
  @Autowired
  private UserRep userRep;
  @Autowired
  VoteService voteService;
  @Autowired
  private RestaurantRep restaurantRep;

  LocalDateTime localDateTimeBefore11 = LocalDateTime.of(LocalDate.now(), LocalTime.parse("10:30:40"));
  LocalDateTime localDateTimeAfter11 = LocalDateTime.of(LocalDate.now(), LocalTime.parse("11:30:40"));

  @Test
  public void testDoubleVoteBefore11(){

    //localDateTimeBefore11.w
    voteService.vote(100002, 100000, localDateTimeBefore11);
    voteService.vote(100003, 100000, localDateTimeBefore11);
    LOG.debug("testDoubleVoteBefore11");

  }

  @Test(expected = RuntimeException.class)
  public void testDoubleVoteAfter11(){

    voteService.vote(100002, 100000, localDateTimeBefore11);
    voteService.vote(100003, 100000, localDateTimeAfter11);
  }

  @Test
  public void vote() throws Exception {
    Map<Integer, Integer> restaurants = new HashMap<>();
    Map<Integer, Integer> users = new HashMap<>();
    for (int i = 0; i < 20; i++) {
      Restaurant restaurant = new Restaurant("Restaurant"+i);
      restaurant = restaurantRep.save(restaurant);
      restaurants.put(i, restaurant.getId());
    }

    for (int i = 0; i < 100; i++) {
      User user = new User("User"+i, "pas"+i, "assa", Role.ROLE_USER);
      user = userRep.save(user);
      users.put(i, user.getId());
    }
    long l = System.currentTimeMillis();
    for (int i = 0; i < 100; i++) {
      voteService.vote(restaurants.get(new Random().nextInt(20)), users.get(i));
    }
    voteService.saveCache();
    System.out.println("saving time: "+ (System.currentTimeMillis()-l));
  }

}