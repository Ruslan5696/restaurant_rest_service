package ru.nocompany.restaurant.web.controller;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.PostConstruct;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import ru.nocompany.restaurant.AbstractTest;
import ru.nocompany.restaurant.model.entities.User;
import ru.nocompany.restaurant.service.UserService;


@WebAppConfiguration
public class RestaurantControllerTest extends AbstractTest {
  private static String RESTAURANT_URL = "/restaurants/";

//  private static final CharacterEncodingFilter CHARACTER_ENCODING_FILTER = new CharacterEncodingFilter();
//
//  static {
//    CHARACTER_ENCODING_FILTER.setEncoding("UTF-8");
//    CHARACTER_ENCODING_FILTER.setForceEncoding(true);
//  }


  protected MockMvc mockMvc;

  @Autowired
  protected UserService userService;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @PostConstruct
  private void postConstruct() {
    mockMvc = MockMvcBuilders
        .webAppContextSetup(webApplicationContext)
        .apply(springSecurity())
        .build();
  }


  @Test
  public void getAll() throws Exception {
    mockMvc.perform(get(RESTAURANT_URL).with(userHttpBasic(USER))).andDo(print()).andExpect(status().isOk());
  }

  @Test
  public void vote() throws Exception {
  }



}