package ru.nocompany.restaurant.web.controller;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.nocompany.restaurant.AbstractTest;
import ru.nocompany.restaurant.model.entities.MenuItem;
import ru.nocompany.restaurant.repository.DishRep;
import ru.nocompany.restaurant.repository.MenuItemRep;
import ru.nocompany.restaurant.service.MenuItemService;
import ru.nocompany.restaurant.service.UserService;
import ru.nocompany.restaurant.web.json.JacksonObjectMapper;

@WebAppConfiguration
@Transactional
public class AdminControllerTest extends AbstractTest {

  private static String RESTAURANT_URL = "/admin/restaurants/";

  protected MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private DishRep dishRep;

  @Autowired
  private MenuItemRep menuItemRep;

  @PostConstruct
  private void postConstruct() {
    mockMvc = MockMvcBuilders
        .webAppContextSetup(webApplicationContext)
        .apply(springSecurity())
        .build();
  }

  @Autowired
  protected UserService userService;

  @Test
  public void addDishes() throws Exception {
    int restaurantId = 100002;
    Integer[] dishIds = {100005, 100006, 100007};
    mockMvc.perform(post(RESTAURANT_URL+restaurantId+"/menuForToday/addDishes")
        .contentType(MediaType.APPLICATION_JSON)
        .content(JacksonObjectMapper.getMapper().writeValueAsString(dishIds))
        .with(userHttpBasic(USER))).andDo(print()).andExpect(status().isOk());
    List<MenuItem> menuItems = menuItemRep
        .getByRestaurantIdAndDate(restaurantId, LocalDate.now());
    List<Integer> dishIdsFromRep = menuItems.stream().map(mi -> mi.getDish().getId())
        .collect(Collectors.toList());
    Assert.assertTrue(Arrays.asList(dishIds).containsAll(dishIdsFromRep));
  }

}