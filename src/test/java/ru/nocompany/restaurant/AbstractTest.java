
package ru.nocompany.restaurant;

import java.util.Arrays;
import org.junit.runner.RunWith;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.model.entities.Role;
import ru.nocompany.restaurant.model.entities.User;

@ContextConfiguration({"classpath:spring/spring_mvc.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractTest {

  protected static User USER = new User();

  static {
    USER.setId(100000);
    USER.setName("User");
    USER.setEmail("user@yandex.ru");
    USER.setEnabled(true);
    USER.setPassword("password");
    USER.setRoles(Arrays.asList(Role.ROLE_USER));
  }

  protected static Restaurant RESTAURANT1 = new Restaurant();

  static {
    RESTAURANT1.setId(100002);
    RESTAURANT1.setName("Restaurant1");
  }

  protected static RequestPostProcessor userHttpBasic(User user) {
    return SecurityMockMvcRequestPostProcessors.httpBasic(user.getEmail(), user.getPassword());
  }
}