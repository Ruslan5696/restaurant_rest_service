package ru.nocompany.restaurant.repository.impl.jpa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.nocompany.restaurant.AbstractTest;
import ru.nocompany.restaurant.model.entities.Dish;
import ru.nocompany.restaurant.repository.DishRep;

@WebAppConfiguration
public class DishRepImplTest extends AbstractTest{

  @Autowired
  DishRep dishRep;

  @Test
  public void getReferencesById() throws Exception {
    int dishId1 = 100005;
    int dishId2 = 100006;
    int dishId3 = 100007;
    List<Integer> integerList =new ArrayList<>();
    integerList.add(dishId1);
    integerList.add(dishId2);
    integerList.add(dishId3);
    List<Dish> referencesById = dishRep.getReferencesById(integerList);
    List<Integer> ids = referencesById.stream().map(r -> r.getId())
        .collect(Collectors.toList());
    Assert.assertTrue(ids.contains(dishId1));
    Assert.assertTrue(ids.contains(dishId2));
    Assert.assertTrue(ids.contains(dishId3));
  }

}