package ru.nocompany.restaurant.repository.impl.jpa;

import ru.nocompany.restaurant.AbstractTest;
import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.model.entities.Role;
import ru.nocompany.restaurant.model.entities.User;
import ru.nocompany.restaurant.model.entities.Vote;
import ru.nocompany.restaurant.repository.VoteRep;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@WebAppConfiguration
public class VoteRepImplTest extends AbstractTest {

  @Test
  public void save() throws Exception {
  }

  @Test
  public void testSaveCollection() throws Exception {
    List<Vote> votes = new ArrayList<>();
    for (int i = 0; i < 1000; i++) {
      votes.add(new Vote(new User(1, "as", "as", "ff", Role.ROLE_USER), new Restaurant(1, "22"),
          LocalDateTime.now()));
    }
    Long beforeSave = System.currentTimeMillis();
    for (Vote vote : votes) {
      voteRep.save(vote);
    }
    System.out.println("time: " + (System.currentTimeMillis() - beforeSave));
    beforeSave = System.currentTimeMillis();
    voteRep.save(votes);
    System.out.println("time: " + (System.currentTimeMillis() - beforeSave));
  }

  @Test
  public void getLastByUserId() throws Exception {
  }

  @Autowired
  private VoteRep voteRep;

  @Test
  public void testGetByUserIdForToday() {
    Vote vote = new Vote(new User(100001, "as", "as", "ff", Role.ROLE_USER),
        new Restaurant(100002, "22"), LocalDateTime.now());
    voteRep.save(vote);
    Assert.assertEquals(vote, voteRep.getLastForTodayByUserId(100001));
  }


  @Test
  public void testSaveAndGetLastByUserId() throws Exception {
    Vote vote = new Vote();
    vote.setRestaurant(RESTAURANT1);
    vote.setUser(USER);
    LocalDateTime dateTime = LocalDateTime.now();
    vote.setDateTime(dateTime);
    voteRep.save(vote);
    Vote lastByUserId = voteRep.getLastByUserId(USER.getId());
    Assert.assertEquals(dateTime, lastByUserId.getDateTime());

  }

}