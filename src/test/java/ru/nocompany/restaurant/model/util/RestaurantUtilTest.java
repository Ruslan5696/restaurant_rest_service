package ru.nocompany.restaurant.model.util;

import ru.nocompany.restaurant.AbstractTest;
import ru.nocompany.restaurant.model.util.RestaurantUtil;
import java.time.LocalDate;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.repository.MenuItemRep;
import ru.nocompany.restaurant.repository.RestaurantRep;
import ru.nocompany.restaurant.service.MenuItemService;



public class RestaurantUtilTest extends AbstractTest {

  @Autowired
  RestaurantRep restaurantRep;
  @Autowired
  MenuItemService menuItemService;
  @Autowired
  MenuItemRep menuItemRep;

//  @Test
//  public void getRestaurantWithDishes() throws Exception {
//    System.out.println(RestaurantUtil.getRestaurantWithDishes(restaurantRep.getById(100002),
//        menuItemService.getByRestaurantIdAndDateWithDishes(100002, LocalDate.parse("2015-05-30"))));
//  }

  @Test
  public void getRestaurantWithDishesRep() throws Exception {
    Restaurant byId = restaurantRep.getReferenceById(100002);
    Integer id = byId.getId();
    System.out.println(RestaurantUtil.getRestaurantWithDishes(restaurantRep.getById(100002),
        menuItemRep.getByRestaurantIdAndDateWithDishes(100002, LocalDate.parse("2015-05-30"))));
  }

}