DROP TABLE IF EXISTS user_roles;
DROP TABLE IF EXISTS menu_items;
DROP TABLE IF EXISTS vote;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS restaurant;
DROP TABLE IF EXISTS dishes;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START 100000;
CREATE TABLE users
(
  id               INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  name             VARCHAR                  NOT NULL,
  email            VARCHAR                  NOT NULL,
  password         VARCHAR                  NOT NULL,
  registered       TIMESTAMP DEFAULT now()  NOT NULL,
  enabled          BOOL DEFAULT TRUE        NOT NULL
);
CREATE UNIQUE INDEX users_unique_email_idx ON users (email);

CREATE TABLE user_roles
(
  user_id INTEGER NOT NULL,
  role    VARCHAR,
  CONSTRAINT user_roles_idx UNIQUE (user_id, role),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
CREATE TABLE restaurant (
  id          INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  name     VARCHAR   NOT NULL
);

CREATE TABLE dishes (
  id          INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  name        VARCHAR   NOT NULL,
  price       INTEGER   NOT NULL
);

CREATE TABLE menu_items (
  id          INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  rest_id     INTEGER NOT NULL,
  dish_id     INTEGER NOT NULL,
  date        TIMESTAMP NOT NULL,
  CONSTRAINT rest_date_dish_idx UNIQUE (rest_id, date, dish_id),
  FOREIGN KEY (rest_id) REFERENCES restaurant (id) ON DELETE CASCADE,
  FOREIGN KEY (dish_id) REFERENCES dishes (id) ON DELETE CASCADE

);
CREATE TABLE vote (
  id          INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  user_id     INTEGER   NOT NULL,
  rest_id     INTEGER   NOT NULL,
  date_time   TIMESTAMP NOT NULL,
  FOREIGN KEY (rest_id) REFERENCES restaurant (id) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
)