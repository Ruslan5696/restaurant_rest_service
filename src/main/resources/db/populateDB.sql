DELETE FROM user_roles;
DELETE FROM menu_items;
DELETE FROM vote;
DELETE FROM users;
DELETE FROM restaurant;
DELETE FROM dishes;
ALTER SEQUENCE global_seq RESTART WITH 100000;

INSERT INTO users (name, email, password)
VALUES ('User', 'user@yandex.ru', 'password');

INSERT INTO users (name, email, password)
VALUES ('Admin', 'admin@gmail.com', 'password');

INSERT INTO restaurant (name) VALUES
  ('Restaurant1'),
  ('Restaurant2'),
  ('Restaurant3');

INSERT INTO user_roles (role, user_id) VALUES
  ('ROLE_USER', 100000),
  ('ROLE_ADMIN', 100001),
  ('ROLE_USER', 100001);

INSERT INTO dishes (name, price) VALUES
  ('Чай', 150), --5
  ('Кофе', 250),
  ('Компот', 250),
  ('Уха', 250), --8
  ('Суп', 250),
  ('Борщ', 250),
  ('Гречка', 300), --11
  ('Макароны', 300),
  ('Рис', 300);

INSERT INTO menu_items (rest_id, dish_id, date) VALUES
  (100002, 100005, '2015-05-30'),
  (100002, 100008, '2015-05-30'),
  (100002, 100011, '2015-05-30'),
  (100003, 100006, '2015-05-30'),
  (100003, 100009, '2015-05-30'),
  (100003, 100012, '2015-05-30'),
  (100004, 100007, '2015-05-30'),
  (100004, 100010, '2015-05-30'),
  (100004, 100013, '2015-05-30');

INSERT INTO vote (user_id, rest_id, date_time) VALUES
  (100000, 100002, '2015-03-30 12:00:00'),
  (100000, 100003, '2015-04-30 12:00:00'),
  (100000, 100004, '2015-05-30 12:00:00'),
  (100001, 100002, '2015-03-30 12:00:00'),
  (100001, 100003, '2015-04-30 12:00:00'),
  (100001, 100004, '2015-05-30 12:00:00');