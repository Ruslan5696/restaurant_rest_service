package ru.nocompany.restaurant.model.util;

import ru.nocompany.restaurant.model.entities.Restaurant;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import ru.nocompany.restaurant.model.entities.Dish;
import ru.nocompany.restaurant.model.entities.MenuItem;
import ru.nocompany.restaurant.model.to.RestaurantWithDishes;

public class RestaurantUtil {
  public static RestaurantWithDishes getRestaurantWithDishes(Restaurant restaurant, List<MenuItem> menuItems){
    LocalDate date;
    List<Dish> dishes;
    if (menuItems!=null && menuItems.size()>0) {
      date = menuItems.get(0).getDate();
      dishes = menuItems.stream().map(menuItem -> menuItem.getDish()).collect(Collectors.toList());
      return new RestaurantWithDishes(restaurant.getName(), restaurant.getId(), dishes, date);
    }
    return null;
  }

}
