package ru.nocompany.restaurant.model.to;

import ru.nocompany.restaurant.model.entities.Dish;
import java.time.LocalDate;
import java.util.List;

public class RestaurantWithDishes {
  private String name;
  private Integer id;
  private List<Dish> menu;
  private LocalDate date;

  public String getName() {
    return name;
  }

  public RestaurantWithDishes(String name, Integer id, List<Dish> menu, LocalDate date) {
    this.name = name;
    this.id = id;
    this.menu = menu;
    this.date = date;
  }

  @Override
  public String toString() {
    return "RestaurantWithDishes{" +
        "name='" + name + '\'' +
        ", id=" + id +
        ", menu=" + menu +
        ", date=" + date +
        '}';
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public List<Dish> getMenu() {
    return menu;
  }

  public void setMenu(List<Dish> menu) {
    this.menu = menu;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }
}
