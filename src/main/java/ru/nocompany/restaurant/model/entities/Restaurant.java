package ru.nocompany.restaurant.model.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "restaurant")
public class Restaurant extends AbstractNamedEntity{

  public Restaurant(Integer id, String name) {
    super(id, name);
  }

  public Restaurant(String name) {
    super(null, name);
  }

  public Restaurant() {
  }
}
