package ru.nocompany.restaurant.model.entities;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import org.hibernate.Hibernate;
import org.springframework.data.domain.Persistable;

@MappedSuperclass
public abstract class AbstractEntity implements Persistable<Integer> {

  public static final int START_SEQ = 100000;

  @Id
  @SequenceGenerator(name = "global_seq", sequenceName = "global_seq", allocationSize = 1, initialValue = START_SEQ)
  //    @Column(name = "id", unique = true, nullable = false, columnDefinition = "integer default nextval('global_seq')")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "global_seq")
  // PROPERTY access for id due to bug: https://hibernate.atlassian.net/browse/HHH-3718
  @Access(value = AccessType.PROPERTY)
  private Integer id;

  protected AbstractEntity() {
  }

  protected AbstractEntity(Integer id) {
    this.id = id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public Integer getId() {
    return id;
  }

  @Override
  public boolean isNew() {
    return this.id == null;
  }

  @Override
  public String toString() {
    return String.format("Entity %s (%s)", getClass().getName(), getId());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || !getClass().equals(Hibernate.getClass(o))) {
      return false;
    }
    AbstractEntity that = (AbstractEntity) o;
    return getId() != null && getId().equals(that.getId());
  }

  @Override
  public int hashCode() {
    return (getId() == null) ? 0 : getId();
  }
}
