package ru.nocompany.restaurant.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dishes")
public class Dish extends AbstractNamedEntity {
  @Column(name = "price", nullable = false)
  private int price;

  @Override
  public String toString() {
    return "Dish{" +
        "price=" + price +
        ", name='" + name + '\'' +
        '}';
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
}
