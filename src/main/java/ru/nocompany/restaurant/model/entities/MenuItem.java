package ru.nocompany.restaurant.model.entities;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "menu_items", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"rest_id", "dish_id", "date"}, name = "rest_date_dish_idx")})
public class MenuItem extends AbstractEntity {

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "rest_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Restaurant restaurant;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "dish_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Dish dish;

  @Column(name = "date")
  private LocalDate date;

  public MenuItem() {
  }

  public MenuItem(Restaurant restaurant, Dish dish, LocalDate date) {
    this.restaurant = restaurant;
    this.dish = dish;
    this.date = date;
  }

  public Restaurant getRestaurant() {
    return restaurant;
  }

  public void setRestaurant(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  public Dish getDish() {
    return dish;
  }

  public void setDish(Dish dish) {
    this.dish = dish;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }
}
