package ru.nocompany.restaurant.model.entities;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "vote")
public class Vote extends AbstractEntity {

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private User user;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "rest_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Restaurant restaurant;

  @Column(name = "date_time", nullable = false)
  private LocalDateTime dateTime;

  public Vote() {
  }

  public Vote(User user, Restaurant restaurant, LocalDateTime DateTime) {
    this.user = user;
    this.restaurant = restaurant;
    this.dateTime = DateTime;
  }

  @Override
  public String toString() {
    return "Vote{" +
        "user id=" + user.getId() +
        ", restaurant id=" + restaurant.getId() +
        ", dateTime=" + dateTime +
        '}';
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Restaurant getRestaurant() {
    return restaurant;
  }

  public void setRestaurant(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public void setDateTime(LocalDateTime localDateTime) {
    this.dateTime = localDateTime;
  }
}
