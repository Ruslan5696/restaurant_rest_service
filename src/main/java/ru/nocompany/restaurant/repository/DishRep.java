package ru.nocompany.restaurant.repository;

import java.util.List;
import ru.nocompany.restaurant.model.entities.Dish;

public interface DishRep {
  List<Dish> getByIds(List<Integer> ids);
  List<Dish> getReferencesById(List<Integer> ids);
  List<Dish> getAll();
}
