package ru.nocompany.restaurant.repository;

import ru.nocompany.restaurant.model.entities.User;

public interface UserRep {
  User getById(int id);
  User getReferenceById(int id);
  User getByEmail(String email);
  User save(User user);
  boolean delete(int id);

}
