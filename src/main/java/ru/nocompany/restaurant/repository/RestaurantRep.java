package ru.nocompany.restaurant.repository;

import java.util.List;
import ru.nocompany.restaurant.model.entities.Restaurant;

public interface RestaurantRep {
  void delete(int id);
  Restaurant getById(int id);
  List<Restaurant> getAll();
  Restaurant save(Restaurant restaurant);
  Restaurant getReferenceById(int id);
}
