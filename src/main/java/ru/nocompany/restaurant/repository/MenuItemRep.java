package ru.nocompany.restaurant.repository;

import java.time.LocalDate;
import java.util.List;
import ru.nocompany.restaurant.model.entities.MenuItem;

public interface MenuItemRep {
  List<MenuItem> getByRestaurantIdAndDate(Integer integer, LocalDate date);
  void save(List<MenuItem> menuItems);
  MenuItem save(MenuItem menuItem);
  List<MenuItem> getByRestaurantIdAndDateWithDishes(Integer id, LocalDate date);
}
