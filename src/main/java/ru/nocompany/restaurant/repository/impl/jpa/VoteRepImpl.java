package ru.nocompany.restaurant.repository.impl.jpa;

import java.time.LocalDate;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.nocompany.restaurant.model.entities.Vote;
import ru.nocompany.restaurant.repository.VoteRep;

@Repository
@Transactional
public class VoteRepImpl implements VoteRep {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public Vote save(Vote vote) {
    if (vote.isNew()) {
      entityManager.persist(vote);
      return vote;
    } else {
      return entityManager.merge(vote);
    }
  }

  @Override
  public void save(Collection<Vote> votes) {
    votes.forEach(x->save(x));
  }

  @Override
  @Deprecated
  public Vote getLastByUserId(int userId) {
    Vote result = entityManager.createQuery(
        "select v FROM Vote v WHERE v.user.id=:userId and v.dateTime ="
            + " (SELECT max (v2.dateTime) FROM Vote v2 WHERE v2.user.id=:userId)", Vote.class)
        .setParameter("userId", userId)
        .getSingleResult();
    return result;
  }

  @Override
  public Vote getLastForTodayByUserId(int userId) {
    Vote result = null;

      result = entityManager.createQuery(
          "select v FROM Vote v WHERE v.user.id=:userId and v.dateTime ="
              + " (SELECT max (v2.dateTime) FROM Vote v2 WHERE v2.user.id=:userId) and v.dateTime > :today", Vote.class)
          .setParameter("userId", userId)
          .setParameter("today", LocalDate.now().atStartOfDay())
          .getSingleResult();
    return result;
  }
}
