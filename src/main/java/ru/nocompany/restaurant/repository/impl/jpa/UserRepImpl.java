package ru.nocompany.restaurant.repository.impl.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.nocompany.restaurant.model.entities.User;
import ru.nocompany.restaurant.repository.UserRep;
@Repository
@Transactional
public class UserRepImpl implements UserRep {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public User getById(int id) {
    return entityManager.find(User.class, id);
  }

  @Override
  public User getReferenceById(int id) {
    return entityManager.getReference(User.class, id);
  }

  @Override
  public User getByEmail(String email) {
    return entityManager.createQuery("select u from User u LEFT JOIN FETCH u.roles WHERE u.email=:email", User.class).setParameter("email", email).getSingleResult();
  }

  @Override
  public User save(User user) {
    if (user.isNew()){
      entityManager.persist(user);
      return user;
    } else {
      return entityManager.merge(user);
    }
  }

  @Override
  public boolean delete(int id) {
    return entityManager.createQuery("DELETE FROM User u WHERE u.id=:id").setParameter("id", id).executeUpdate()!=0;
  }
}
