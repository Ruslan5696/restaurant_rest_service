package ru.nocompany.restaurant.repository.impl.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.repository.RestaurantRep;
@Repository
@Transactional
public class RestaurantRepImpl implements RestaurantRep  {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public void delete(int id) {
    Restaurant reference = entityManager.getReference(Restaurant.class, id);
    entityManager.remove(reference);
  }

  @Override
  public Restaurant getById(int id) {
    Restaurant restaurant = entityManager.find(Restaurant.class, id);
    return restaurant;
  }

  public Restaurant getReferenceById(int id) {
    return entityManager.getReference(Restaurant.class, id);
  }

  @Override
  public List<Restaurant> getAll() {
    List<Restaurant> restaurants = entityManager.createQuery("SELECT r FROM Restaurant r").getResultList();
    return restaurants;
  }

  @Override
  public Restaurant save(Restaurant restaurant) {
    if (restaurant.isNew()){
      entityManager.persist(restaurant);
      return restaurant;
    } else {
      return entityManager.merge(restaurant);
    }
  }
}
