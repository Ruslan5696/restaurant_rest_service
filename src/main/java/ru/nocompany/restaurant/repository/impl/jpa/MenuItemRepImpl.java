package ru.nocompany.restaurant.repository.impl.jpa;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.nocompany.restaurant.model.entities.MenuItem;
import ru.nocompany.restaurant.repository.MenuItemRep;

@Repository
@Transactional
public class MenuItemRepImpl implements MenuItemRep {
  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<MenuItem> getByRestaurantIdAndDate(Integer id, LocalDate date) {
    List<MenuItem> menuItems = entityManager.createQuery("SELECT mi FROM MenuItem mi WHERE mi.restaurant.id=:id and mi.date=:date")
        .setParameter("id", id)
        .setParameter("date", date).getResultList();
    return menuItems;
  }

  @Override
  public void save(List<MenuItem> menuItems) {
    menuItems.forEach(m->save(m));
  }

  @Override
  public MenuItem save(MenuItem menuItem) {
    if (menuItem.isNew()) {
      entityManager.persist(menuItem);
      return menuItem;
    } else {
      return entityManager.merge(menuItem);
    }
  }

  @Override
  public List<MenuItem> getByRestaurantIdAndDateWithDishes(Integer id, LocalDate date) {
    List<MenuItem> menuItems = entityManager.createQuery("SELECT mi FROM MenuItem mi left join fetch mi.dish WHERE mi.restaurant.id=:id and mi.date=:date")
        .setParameter("id", id)
        .setParameter("date", date).getResultList();
    return menuItems;
  }
}
