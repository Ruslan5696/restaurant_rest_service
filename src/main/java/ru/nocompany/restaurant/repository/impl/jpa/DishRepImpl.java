package ru.nocompany.restaurant.repository.impl.jpa;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import ru.nocompany.restaurant.model.entities.Dish;
import ru.nocompany.restaurant.repository.DishRep;

@Repository
public class DishRepImpl implements DishRep {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<Dish> getByIds(List<Integer> ids) {
    List<Dish> dishes = entityManager.createQuery("SELECT d FROM Dish d WHERE d.id in (:ids)")
        .setParameter("ids", ids).getResultList();
    return dishes;
  }

  @Override
  public List<Dish> getReferencesById(List<Integer> ids) {
    List<Dish> dishes = ids.stream().map(dishId -> entityManager.getReference(Dish.class, dishId))
        .collect(
            Collectors.toList());
    return dishes;
  }

  @Override
  public List<Dish> getAll() {
    return entityManager.createQuery("SELECT d FROM Dish d").getResultList();
  }
}
