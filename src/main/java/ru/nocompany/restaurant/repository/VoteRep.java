package ru.nocompany.restaurant.repository;

import java.util.Collection;
import ru.nocompany.restaurant.model.entities.Vote;

public interface VoteRep {
  Vote save(Vote vote);
  void save(Collection<Vote> votes);
  Vote getLastByUserId(int userId);
  Vote getLastForTodayByUserId(int userId);

}
