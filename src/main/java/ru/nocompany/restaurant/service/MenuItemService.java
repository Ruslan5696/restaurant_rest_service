package ru.nocompany.restaurant.service;

import java.time.LocalDate;
import java.util.List;
import ru.nocompany.restaurant.model.entities.MenuItem;

public interface MenuItemService {
  List<MenuItem> getByRestaurantIdAndDateWithDishes(Integer integer, LocalDate date);
  List<MenuItem> getByRestaurantIdAndDate(Integer integer, LocalDate date);

}
