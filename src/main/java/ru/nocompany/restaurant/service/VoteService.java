package ru.nocompany.restaurant.service;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface VoteService {
  void vote(int restaurantId, int userId);
  void vote(int restaurantId, int userId, LocalDateTime dateTime);
  int countAllByRestaurantId(int restaurantId);
  int countAllByRestaurantIdAndDate(int restaurantId, LocalDate date);
  void saveCache();
}
