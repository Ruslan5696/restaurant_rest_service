package ru.nocompany.restaurant.service;

import ru.nocompany.restaurant.model.entities.User;

public interface UserService {
  User getById(int Id);
  User getByEmail(String email);
  User create(User user);
  User update(User user);
  boolean delete(int id);

}
