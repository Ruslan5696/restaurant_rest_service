package ru.nocompany.restaurant.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.model.entities.User;
import ru.nocompany.restaurant.model.entities.Vote;
import ru.nocompany.restaurant.repository.RestaurantRep;
import ru.nocompany.restaurant.repository.UserRep;
import ru.nocompany.restaurant.repository.VoteRep;
import ru.nocompany.restaurant.service.VoteService;

@Service
public class VoteServiceImpl implements VoteService {
  private static final Logger LOG = LoggerFactory.getLogger(VoteServiceImpl.class) ;


  @Autowired
  private RestaurantRep restaurantRep;

  @Autowired
  private UserRep userRep;

  @Autowired
  private VoteRep voteRep;

  private Map<Integer, Vote> voteCacheMap = new ConcurrentHashMap<>();


  public static void main(String[] args) {
    int i = 5;
    i = ++i + ++i;
    System.out.println(i);
  }

  @Override
  public void vote(int restaurantId, int userId) {

    vote(restaurantId, userId, LocalDateTime.now());
  }

  @Override
  public void vote(int restaurantId, int userId, LocalDateTime dateTime) {

    Restaurant restaurant = restaurantRep.getReferenceById(restaurantId);
    User user = userRep.getReferenceById(userId);
    LocalDateTime voteDateTime = dateTime;
    Vote vote = new Vote(user, restaurant, voteDateTime);
    save(vote);
  }

  @Override
  public int countAllByRestaurantId(int restaurantId) {
    return 0;
  }

  @Override
  public int countAllByRestaurantIdAndDate(int restaurantId, LocalDate date) {
    return 0;
  }

  private void save(Vote vote) {
    Vote alreadyVoted = getLastForTodayByUserId(vote.getUser().getId());
    if (alreadyVoted != null) {
      if (vote.getDateTime().toLocalTime().isAfter(LocalTime.parse("11:00:00"))){
        LOG.debug("vote after 11");
        throw new RuntimeException("after 11 cant change vote");
      }
      vote.setId(alreadyVoted.getId());
    }
    voteCacheMap.put(vote.getUser().getId(), vote);
    if (voteCacheMap.size()>1000){
      saveCache();
    }
//    voteRep.getLastByUserId(vote.getUser().getId());
//
//    voteCacheMap.put(vote.getUser().getId(), vote);
//    if (voteCacheMap.size() > 1000) {
//      saveCache();
//    }
  }

  private void saveWithoutCache(Vote vote) {
    Vote alreadyVoted = null;
    try {
      alreadyVoted = voteRep.getLastForTodayByUserId(vote.getUser().getId());
    } catch (Exception e) {
    }
    if (alreadyVoted != null) {
      if (vote.getDateTime().toLocalTime().isAfter(LocalTime.parse("11:00:00"))){

        throw new RuntimeException("after 11 cant change vote");
      }
      vote.setId(alreadyVoted.getId());
    }
    voteRep.save(vote);
  }

  private Vote getLastForTodayByUserId(int id){
    Vote alreadyVoted = null;
    if ((alreadyVoted = voteCacheMap.get(id))!=null) {
      LOG.debug("Get vote from cache {}", alreadyVoted);
      return alreadyVoted;
    }
    try {
      alreadyVoted = voteRep.getLastForTodayByUserId(id);
      LOG.debug("Get vote from db {}", alreadyVoted);
//      voteCacheMap.put(alreadyVoted.getUser().getId(), alreadyVoted);
    } catch (Exception e) {
      LOG.debug("Exist vote didnt find", alreadyVoted);
      return null;
    }
    return alreadyVoted;
  }

  public synchronized void saveCache() {
    if (voteCacheMap.size() > 0) {
      voteRep.save(voteCacheMap.values());
      voteCacheMap.clear();
    }
  }


}
