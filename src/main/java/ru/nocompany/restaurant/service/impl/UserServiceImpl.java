package ru.nocompany.restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.nocompany.restaurant.AuthorizedUser;
import ru.nocompany.restaurant.model.entities.User;
import ru.nocompany.restaurant.repository.UserRep;
import ru.nocompany.restaurant.service.UserService;
@Service
public class UserServiceImpl implements UserService, UserDetailsService {
  @Autowired
  private UserRep userRep;

  @Override
  public User getById(int id) {
    return userRep.getById(id);
  }

  @Override
  public User getByEmail(String email) {
    return userRep.getByEmail(email);
  }

  @Override
  public User create(User user) {
    return userRep.save(user);
  }

  @Override
  public User update(User user) {
    return userRep.save(user);
  }

  @Override
  public boolean delete(int id) {
    return userRep.delete(id);
  }

  @Override
  public AuthorizedUser loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = userRep.getByEmail(email.toLowerCase());
    if (user == null) {
      throw new UsernameNotFoundException("User " + email + " is not found");
    }
    return new AuthorizedUser(user);
  }
}
