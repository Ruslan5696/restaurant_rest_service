package ru.nocompany.restaurant.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nocompany.restaurant.model.entities.Dish;
import ru.nocompany.restaurant.model.entities.MenuItem;
import ru.nocompany.restaurant.repository.DishRep;
import ru.nocompany.restaurant.repository.MenuItemRep;
import ru.nocompany.restaurant.service.MenuItemService;

@Service
@Deprecated
public class MenuItemServiceImpl implements MenuItemService {
  @Autowired
  MenuItemRep menuItemRep;
  @Autowired
  DishRep dishRep;
  @Override
  public List<MenuItem> getByRestaurantIdAndDateWithDishes(Integer integer, LocalDate date) {
    List<MenuItem> menuItems = menuItemRep.getByRestaurantIdAndDate(integer, date);
    List<Integer> dishIds = menuItems.stream().map(menuItem -> menuItem.getDish().getId()).collect(
        Collectors.toList());
    List<Dish> dishes = dishRep.getByIds(dishIds);
    Map<Integer, Dish> dishMap = dishes.stream().collect(Collectors.toMap(Dish::getId, x->x));
    menuItems.forEach(x->x.setDish(dishMap.get(x.getDish().getId())));
    return menuItems;
  }

  @Override
  public List<MenuItem> getByRestaurantIdAndDate(Integer integer, LocalDate date) {
    return null;
  }
}
