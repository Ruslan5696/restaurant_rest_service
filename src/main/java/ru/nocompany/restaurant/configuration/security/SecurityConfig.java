package ru.nocompany.restaurant.configuration.security;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import ru.nocompany.restaurant.service.impl.UserServiceImpl;

@EnableWebSecurity
public class SecurityConfig {


  @Configuration
  @EnableGlobalMethodSecurity(securedEnabled = true)

  public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

    @Resource
    UserServiceImpl userDetailsService;
    @Autowired
    private HttpAuthenticationEntryPoint authenticationEntryPoint;

    protected void configure(HttpSecurity http) throws Exception {
      http.csrf().disable()
          .userDetailsService(userDetailsService)
          .authorizeRequests()
          .antMatchers("/rest/restaurants/**").hasRole("USER")
          .antMatchers("/rest/admin/restaurants/**").hasRole("ADMIN")
          .and()
          .httpBasic()
          .authenticationEntryPoint(authenticationEntryPoint);
    }


  }

  @Configuration
  @Order(1)
  public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Resource
    UserServiceImpl userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http
          .userDetailsService(userDetailsService)
          .authorizeRequests()
          .antMatchers("/resources/**", "/webjars/**").permitAll()
          .antMatchers("/login", "/register").anonymous()
          .antMatchers("/**/admin/**").hasRole("ADMIN")
          .antMatchers("/**").authenticated()
          .and()
          .formLogin()
          .loginPage("/login").failureUrl("/login?error=true")
          .defaultSuccessUrl("/restaurants", true)
          .loginProcessingUrl("/spring_security_check")
          .and()
          .logout().logoutSuccessUrl("/login");
    }
  }
}
