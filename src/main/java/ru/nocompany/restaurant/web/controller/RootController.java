package ru.nocompany.restaurant.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {
  private static final Logger LOG = LoggerFactory.getLogger(RootController.class);

  @GetMapping(value = "/")
  public String root(Model model) {
    model.addAttribute("recipient", "World");
    return "redirect:restaurants";
  }

  @GetMapping(value = "/login")
  public String login(Model model) {
    model.addAttribute("recipient", "World");
    return "login.html";
  }
}
