package ru.nocompany.restaurant.web.controller;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.nocompany.restaurant.model.entities.Dish;
import ru.nocompany.restaurant.model.entities.MenuItem;
import ru.nocompany.restaurant.model.entities.Restaurant;
import ru.nocompany.restaurant.repository.DishRep;
import ru.nocompany.restaurant.repository.MenuItemRep;
import ru.nocompany.restaurant.repository.RestaurantRep;

@RestController
@RequestMapping(value = AdminController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminController {

  private static final Logger LOG = LoggerFactory.getLogger(AdminController.class);
  public static final String REST_URL = "/admin/restaurants";

  @Autowired
  RestaurantRep restaurantRep;

  @Autowired
  MenuItemRep menuItemRep;

  @Autowired
  DishRep dishRep;

//  @PostMapping
//  public void addNewRestaurant(@RequestBody Restaurant restaurant){
//    restaurantRep.save(restaurant);
//    LOG.debug("Add restaurant {}", restaurant);
//  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Restaurant> addWithLocation(@Valid @RequestBody Restaurant restaurant) {
    Restaurant created = restaurantRep.save(restaurant);

    URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
        .path(REST_URL + "/{id}")
        .buildAndExpand(created.getId()).toUri();
    LOG.debug("Add restaurant {}", restaurant);
    return ResponseEntity.created(uriOfNewResource).body(created);
  }

  @GetMapping("/{id}")
  public Restaurant get(@PathVariable("id") int id) {
    Restaurant restaurantRepById = restaurantRep.getById(id);
    LOG.debug("get restaurant {}", id);
    return restaurantRepById;
  }


  @GetMapping("/{restaurantId}/menuForToday")
  public List<MenuItem> getMenuForToday(@PathVariable("restaurantId") int restaurantId) {
    LOG.debug("get menu for today for restaurantId = {}", restaurantId);
    return menuItemRep.getByRestaurantIdAndDate(restaurantId, LocalDate.now());
  }

  @PostMapping("/{restaurantId}/menuForToday/addDishes")
  public void addDishes(@PathVariable("restaurantId") int restaurantId,
      @RequestBody Integer[] dishIds) {
    List<MenuItem> menuItems = new ArrayList<>();
    List<Dish> dishes = dishRep.getReferencesById(Arrays.asList(dishIds));
    Restaurant restaurant = restaurantRep.getReferenceById(restaurantId);
    dishes.forEach(dish -> menuItems.add(new MenuItem(restaurant, dish, LocalDate.now())));
    LOG.debug("save {} menu items", menuItems.size());
    menuItemRep.save(menuItems);
  }

  @GetMapping("/dishes")
  public List<Dish> getDishes() {
    LOG.debug("get all dishes");
    return dishRep.getAll();
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable("id") int id) {
    restaurantRep.delete(id);
    LOG.debug("delete restaurant {}", id);
  }

  @GetMapping
  public List<Restaurant> getAll() {
    List<Restaurant> restaurants = restaurantRep.getAll();
    LOG.debug("get all restaurants {}");
    return restaurants;
  }

  @PutMapping
  public void updateRestaurant(@RequestBody Restaurant restaurant) {
    restaurantRep.save(restaurant);
    LOG.debug("Add restaurant {}", restaurant);
  }

}
