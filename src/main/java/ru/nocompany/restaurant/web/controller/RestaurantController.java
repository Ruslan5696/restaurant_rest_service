package ru.nocompany.restaurant.web.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.nocompany.restaurant.AuthorizedUser;
import ru.nocompany.restaurant.model.to.RestaurantWithDishes;
import ru.nocompany.restaurant.model.util.RestaurantUtil;
import ru.nocompany.restaurant.repository.MenuItemRep;
import ru.nocompany.restaurant.repository.RestaurantRep;
import ru.nocompany.restaurant.service.VoteService;

@RestController
@RequestMapping(value = "/restaurants")
//@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class RestaurantController {
  private static final Logger LOG = LoggerFactory.getLogger(RestaurantController.class);

  @Autowired
  private RestaurantRep restaurantRep;
  @Autowired
  private MenuItemRep menuItemRep;
  @Autowired
  private VoteService voteService;

  public static void main(String[] args) {
    int i = 5;
    i = i-- - --i;
    System.out.println(i);
  }

  private static int met() {
    return true ? null : 0;
  }

//  @RequestMapping(value = "/", method = RequestMethod.GET)
//  public String printWelcome(ModelMap model) {
//
//    model.addAttribute("message", "Spring 3 MVC Hello World");
//    return "hello";
//
//  }

  @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<RestaurantWithDishes> getAll() {
    List<RestaurantWithDishes> restaurantWithDishes = new ArrayList<>();
    restaurantRep.getAll()
        .forEach(restaurant -> restaurantWithDishes.add(RestaurantUtil
            .getRestaurantWithDishes(restaurant,
                menuItemRep.getByRestaurantIdAndDateWithDishes(restaurant.getId(),
                    LocalDate.parse("2015-05-30")))));
    System.out.println(restaurantWithDishes);
    return restaurantWithDishes;
  }

  @GetMapping(value = "/{id}")
  public void vote(@PathVariable("id") int id){
    voteService.vote(id, AuthorizedUser.getId());
    LOG.debug("vote to {} from {}", id, AuthorizedUser.safeGet().getUsername());
  }

  @RequestMapping(value = "/hello/{name:.+}", method = RequestMethod.GET)
  public ModelAndView hello(@PathVariable("name") String name) {

    ModelAndView model = new ModelAndView();
    model.setViewName("hello");
    model.addObject("msg", name);

    return model;

  }

}