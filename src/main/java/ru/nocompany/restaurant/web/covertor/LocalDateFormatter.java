package ru.nocompany.restaurant.web.covertor;


import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import org.springframework.format.Formatter;
import org.springframework.util.StringUtils;


public class LocalDateFormatter implements Formatter<LocalDate> {

  @Override
  public LocalDate parse(String text, Locale locale) throws ParseException {
    return StringUtils.isEmpty(text) ? null : LocalDate.parse(text);
  }

  @Override
  public String print(LocalDate lt, Locale locale) {
    return lt.format(DateTimeFormatter.ISO_LOCAL_DATE);
  }
}



